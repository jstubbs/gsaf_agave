# Project files for the GSAF+Agave integration POC project.

## Basic Information

### GSAF servers: 
* deborahseq.icmb.utexas.edu
* prandtlseq.icmb.utexas.edu

### GSAF wikis:
* Data flow, by sequencer: https://wikis.utexas.edu/pages/viewpage.action?title=Data+flow%2C+by+sequencer&spaceKey=GSAFgroup
* Data delivery: https://wikis.utexas.edu/display/GSAFgroup/Data+delivery+system