#!/bin/bash
#input: Unaligned directory path

runID=$1 

################################################################################
#
#	BEGIN: LOCALIZATION VARIABLES
#
################################################################################

ROOTPATH=/raida
USER=sbuser

PROCESSING_SYSTEM=darcy
PROCESSING_STEP="BCL to Fastq conversion"

DATADIR=/corralZ/projects/DNADenovo/illumina_split_data

NEXT_PROCESSING=$ROOTPATH/vamsi/gsaf-script/mongohq/populatedb.bash

TACC_LOGIN=sphsmith@stampede.tacc.utexas.edu
TACC_SCRIPT=rranch.sh

MKDIR=imkdir
PUT=iput
PUT_OPTS=-rfKV
PUT_COMMAND="$PUT $PUT_OPTS"

################################################################################
#
#	END: LOCALIZATION VARIABLES
#
################################################################################

pushd . &> /dev/null

if [ -d $ROOTPATH/$USER/$runID ]
then 
   baseDir=$ROOTPATH/$USER/$runID
else
   if [ -d $ROOTPATH/$USER/$runID ]
   then
      baseDir=$ROOTPATH/$USER/$runID
   else
      echo "ERROR: Run id $runID does not exist on $PROCESSING_SYSTEM."
      exit 1
   fi
fi

cd $baseDir/[12][0-9][0-9][0-9][0-9][0-9]_[A-Z]*/Unaligned
SAdir=`pwd`

tail -1 make.err | grep successfully &> /dev/null
if [ $? != "0" ]
then
   echo "ERROR: $PROCESSING_STEP did not complete successfully for run $runID.  Exiting $0 on `date`"
   exit 1
fi
grep -i error make.err &> /dev/null
if [ $? == "0" ]
then
   echo "ERROR: $PROCESSING_STEP reported an error for run $runID.  Exiting $0 on `date`"
   exit 1
fi

seqRun=`pwd | awk 'BEGIN {FS="/"} {for(i=0;i<=NF;i++) {if (index($i,"SA")==1) {print $i}}}'`
fcid=`pwd | awk 'BEGIN {FS="/"} {for(i=0;i<=NF;i++) {if (index($i,"SA")==1) {print $(i+1)}}}'`

if [ $seqRun != $runID ]
then
   echo "ERROR: $runID does not match $seqRun in $0 on `date`"
   exit 1
fi

$MKDIR $DATADIR/$seqRun
$MKDIR $DATADIR/$seqRun/$fcid

list=`ls -d Project*`
#echo $list

for project in $list
do
  echo "in $project"

   $MKDIR $DATADIR/$seqRun/$project
# Before bcl2fastq 1.8.4, these were .cat.fastq
   $PUT_COMMAND $project/Sample*/*.fastq.gz $DATADIR/$seqRun/$project &>${PUT}.$project.log 
   echo "$MKDIR $DATADIR/$seqRun/$project"
   echo "$PUT_COMMAND $project/Sample*/*.fastq.gz $DATADIR/$seqRun/$project &>${PUT}.$project.log "
done

# wait

$MKDIR $DATADIR/$seqRun/Undetermined_indices
$PUT_COMMAND Undeter*/S*/*.fastq.gz $DATADIR/$seqRun/Undetermined_indices &>${PUT}.undetermined.log &

grep -i err i*log &> /dev/null
if [ $? == "0" ]
then
   echo "*****************"
   echo "WARNING: An error has been detected in an $PUT log file for run $runID $0 on `date`"
   echo "Continuing anyway..."
   echo "*****************"
   # exit 1
fi

md5sum Project_JA*/S*/*.gz Undeter*/S*/l*.gz > md5sums.txt 
# moved to configureBclAndRun 2/14/14 by SPHS
# getSizes.sh Project_JA\*/S\*/\*.gz Undeter\*/S\*/l\*.gz &

# wait

$PUT_COMMAND md5sums.txt $DATADIR/$seqRun/ &>${PUT}.metadata.log 
$PUT_COMMAND sizes.txt $DATADIR/$seqRun/ >>${PUT}.metadata.log 2>&1
$PUT_COMMAND support.txt $DATADIR/$seqRun/ >>${PUT}.metadata.log 2>&1
$PUT_COMMAND sizes $DATADIR/$seqRun/ >>${PUT}.metadata.log 2>&1
$PUT_COMMAND Demu* $DATADIR/$seqRun/ >>${PUT}.metadata.log 2>&1
$PUT_COMMAND make.out $DATADIR/$seqRun/ >>${PUT}.metadata.log 2>&1
$PUT_COMMAND make.err $DATADIR/$seqRun/ >>${PUT}.metadata.log 2>&1

cd ../
for file in `find . -maxdepth 1 -type f -print`; 
do 
   $PUT_COMMAND $file $DATADIR/$seqRun/$fcid/. >> Unaligned/${PUT}.metadata.log 2>&1
done
$PUT_COMMAND Config $DATADIR/$seqRun/$fcid/. >> Unaligned/${PUT}.metadata.log 2>&1
$PUT_COMMAND InterOp $DATADIR/$seqRun/$fcid/. >> Unaligned/${PUT}.metadata.log 2>&1
cd Unaligned

$PUT_COMMAND ${PUT}*log $DATADIR/$seqRun

echo "Checking for errors in logs..."
grep -i err i*log
if [ $? -ne 0 ]
then
   echo "There do not appear to be any errors in the log files.  It is probably safe to continue."
else
   echo "WARNING: There appear to be errors in the log files.  It is probably safe to continue, but you might want to stop anyway."
fi

bash $NEXT_PROCESSING $SAdir 

# This call starts a BACKGROUND process on fourierseq for the rsync
# ssh -i ~/.ssh/id_four_ls scott@fourierseq.icmb.utexas.edu rsyncToRanch.sh $seqRun
# Replaced with this much faster call to do this at TACC
ssh -i $ROOTPATH/$USER/.ssh/id_stamp_emailCustomers $TACC_LOGIN $TACC_SCRIPT $seqRun


# need to verify puts...

# Create email texts
# Send emails using email_customers.sh
#  all in email_customers.sh
# also need to rsync to ranch

popd &> /dev/null
