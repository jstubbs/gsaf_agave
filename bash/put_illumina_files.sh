#! /bin/bash
#input: Unaligned directory path

runID=$1 
pushd . &> /dev/null

if [ -d /raida/sbsuser/$runID ]
then 
   baseDir=/raida/sbsuser/$runID
else
   if [ -d /raidb/sbsuser/$runID ]
   then
      baseDir=/raidb/sbsuser/$runID
   else
      echo "ERROR: Run id $runID does not exist on darcy."
      exit 1
   fi
fi

cd $baseDir/[12][0-9][0-9][0-9][0-9][0-9]_[A-Z]*/Unaligned
SAdir=`pwd`

tail -1 make.err | grep successfully &> /dev/null
if [ $? != "0" ]
then
   echo "ERROR: BCL to Fastq conversion did not complete successfully for run $runID.  Exiting $0 on `date`"
   exit 1
fi
grep -i error make.err &> /dev/null
if [ $? == "0" ]
then
   echo "ERROR: BCL to Fastq conversion reported an error for run $runID.  Exiting $0 on `date`"
   exit 1
fi

seqRun=`pwd | awk 'BEGIN {FS="/"} {for(i=0;i<=NF;i++) {if (index($i,"SA")==1) {print $i}}}'`
fcid=`pwd | awk 'BEGIN {FS="/"} {for(i=0;i<=NF;i++) {if (index($i,"SA")==1) {print $(i+1)}}}'`

if [ $seqRun != $runID ]
then
   echo "ERROR: $runID does not match $seqRun in $0 on `date`"
   exit 1
fi

imkdir /corralZ/projects/DNADenovo/illumina_split_data/$seqRun
imkdir /corralZ/projects/DNADenovo/illumina_split_data/$seqRun/$fcid

list=`ls -d Project*`
#echo $list

for project in $list
do
  echo "in $project"

   imkdir /corralZ/projects/DNADenovo/illumina_split_data/$seqRun/$project
# Before bcl2fastq 1.8.4, these were .cat.fastq
   iput -rfKV $project/Sample*/*.fastq.gz /corralZ/projects/DNADenovo/illumina_split_data/$seqRun/$project &>iput.$project.log 
   echo "imkdir /corralZ/projects/DNADenovo/illumina_split_data/$seqRun/$project"
   echo "iput -rfKV $project/Sample*/*.fastq.gz /corralZ/projects/DNADenovo/illumina_split_data/$seqRun/$project &>iput.$project.log "
done

# wait

imkdir /corralZ/projects/DNADenovo/illumina_split_data/$seqRun/Undetermined_indices
iput -rfKV Undeter*/S*/*.fastq.gz /corralZ/projects/DNADenovo/illumina_split_data/$seqRun/Undetermined_indices &>iput.undetermined.log &

grep -i err i*log &> /dev/null
if [ $? == "0" ]
then
   echo "*****************"
   echo "WARNING: An error has been detected in an iput log file for run $runID $0 on `date`"
   echo "Continuing anyway..."
   echo "*****************"
   # exit 1
fi

md5sum Project_JA*/S*/*.gz Undeter*/S*/l*.gz > md5sums.txt 
# moved to configureBclAndRun 2/14/14 by SPHS
# getSizes.sh Project_JA\*/S\*/\*.gz Undeter\*/S\*/l\*.gz &

# wait

iput -rfKV md5sums.txt /corralZ/projects/DNADenovo/illumina_split_data/$seqRun/ &>iput.metadata.log 
iput -rfKV sizes.txt /corralZ/projects/DNADenovo/illumina_split_data/$seqRun/ >>iput.metadata.log 2>&1
iput -rfKV support.txt /corralZ/projects/DNADenovo/illumina_split_data/$seqRun/ >>iput.metadata.log 2>&1
iput -rfKV sizes /corralZ/projects/DNADenovo/illumina_split_data/$seqRun/ >>iput.metadata.log 2>&1
iput -rfKV Demu* /corralZ/projects/DNADenovo/illumina_split_data/$seqRun/ >>iput.metadata.log 2>&1
iput -rfKV make.out /corralZ/projects/DNADenovo/illumina_split_data/$seqRun/ >>iput.metadata.log 2>&1
iput -rfKV make.err /corralZ/projects/DNADenovo/illumina_split_data/$seqRun/ >>iput.metadata.log 2>&1

cd ../
for file in `find . -maxdepth 1 -type f -print`; 
do 
   iput -KV $file /corralZ/projects/DNADenovo/illumina_split_data/$seqRun/$fcid/. >> Unaligned/iput.metadata.log 2>&1
done
iput -rfKV Config /corralZ/projects/DNADenovo/illumina_split_data/$seqRun/$fcid/. >> Unaligned/iput.metadata.log 2>&1
iput -rfKV InterOp /corralZ/projects/DNADenovo/illumina_split_data/$seqRun/$fcid/. >> Unaligned/iput.metadata.log 2>&1
cd Unaligned

iput -rfKV iput*log /corralZ/projects/DNADenovo/illumina_split_data/$seqRun

echo "Checking for errors in logs..."
grep -i err i*log
if [ $? -ne 0 ]
then
   echo "There do not appear to be any errors in the log files.  It is probably safe to continue."
else
   echo "WARNING: There appear to be errors in the log files.  It is probably safe to continue, but you might want to stop anyway."
fi

bash /raida/vamsi/gsaf-script/mongohq/populatedb.bash $SAdir 

# This call starts a BACKGROUND process on fourierseq for the rsync
# ssh -i ~/.ssh/id_four_ls scott@fourierseq.icmb.utexas.edu rsyncToRanch.sh $seqRun
# Replaced with this much faster call to do this at TACC
ssh -i /raida/sbsuser/.ssh/id_stamp_emailCustomers sphsmith@stampede.tacc.utexas.edu rranch.sh $seqRun


# need to verify iputs...

# Create email texts
# Send emails using email_customers.sh
#  all in email_customers.sh
# also need to rsync to ranch

popd &> /dev/null
